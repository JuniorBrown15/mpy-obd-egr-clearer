import time
from elm327.obd import OBDInterface


class Alfa159Error(Exception):
    pass


class Alfa159:
    def __init__(self, _obd, _logfile):
        """
        :param OBDInterface _obd:
        """
        self.obd = _obd
        self.logfile = _logfile
        self.n_nodata = 0

    def setup(self):
        self.logfile.write("Setup\n")
        self.obd.send_command("AT Z")    # Reset
        self.obd.send_command("AT E0")   # Echo Off
        self.obd.send_command("ATIB10")  # set the ISO Baud rate to 10400
        self.obd.send_command("ATSP5")   # ISO 14230-4 KWP (slow init, 10.4 kbaud) (auto fallback)
        self.obd.send_command("ATS0")    # No spaces
        self.obd.send_command("ATL0")    # No linefeed after carriage return
        time.sleep(1)

        # SH xx yy zz
        # [ Set the Header to xx yy zz ]
        self.obd.send_command('ATSH8110F1')
        # Headers on
        self.obd.send_command('ATH1')
        # allow  long sends  (eight  data  bytes)  and  long  receives  (unlimited in number)
        self.obd.send_command('ATAL')
        
        # Should init the bus
        init = self.obd.send_command("ATFI")    # Fast Init
        self.logfile.write(init.replace('\r', '\n'))
        return init
        # self.obd.send_command('1A97')
        # Display protocol
        # self.obd.send_command('ATDP')

    def check_response(self, retval):
        if not retval.strip():
            self.n_nodata += 1
        elif 'NO DATA' in retval:
            self.n_nodata += 1
        else:
            self.n_nodata = 0

        if self.n_nodata > 5:
            self.n_nodata = 0
            self.sleep()
            return self.reset()

        for err in ('BUS BUSY', 'BUS INIT: ERROR'):
            if err in retval:
                self.obd.send_command('ATPC')
                time.sleep(120)
                return self.reset()

        return retval.replace('BUS INIT: OK', '').strip()

    def reset(self):
        self.obd.send_command('ATPC')
        # Reset programable params
        self.obd.send_command('AT PP FF 0FF')

        self.setup()

        raise Alfa159Error()

    def sleep(self, sleepfor=360):
        time.sleep(sleepfor)
