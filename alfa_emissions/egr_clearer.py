import time
from elm327.obd import OBDInterface
import alfa_emissions


class EgrClearer(alfa_emissions.Alfa159):

    def check_egr_fault(self):
        retval = self.obd.send_command('1800FF001').strip()
        retval = self.check_response(retval)

        retval = retval.split('\r')
        none = '82F1105800DB'
        active = {'85F11058010401F2D6', '85F11058010401E2C6'}

        if len(retval) == 1 and retval[0] in active:
            return True
        elif len(retval) == 1 and retval[0] == none:
            return False
        self.logfile.write("check: %s\n" % (','.join(retval)))

    def clear_fault(self):
        result = self.obd.send_command('14FF001').strip()
        success = '83F11054FF00D7'

        self.logfile.write("EGR Cleared: %s\n" % (result == success))
        return result == success

    def process(self):
        if self.check_egr_fault():
            self.clear_fault()
            return not self.check_egr_fault()
