#
# micropython-serial - pySerial-like interface for MicroPython
# https://github.com/pfalcon/micropython-serial
#
# Copyright (c) 2014 Paul Sokolovsky
# Licensed under MIT license
#
import sys
try:
    import os
    import termios
    import ustruct
    import fcntl
    import uselect
    import glob
    EMBEDDED = False

    FIONREAD = const(0x541b)

except ImportError:
    import machine
    EMBEDDED = True


class SerialException(OSError):
    pass


class SerialDisconnectException(SerialException):
    pass


class SerialCommon(object):
    def __init__(self, port, baudrate, timeout=None, **kwargs):
        self.port = None

    def open(self):
        pass

    def close(self):
        pass

    @property
    def baudrate(self):
        return self._baudrate

    @baudrate.setter
    def baudrate(self, _baudrate):
        self._baudrate = _baudrate
        self.close()
        self.open()

    @property
    def portstr(self):
        return str(self.port)

    @property
    def in_waiting(self):
        return 0

    def inWaiting(self):
        return self.in_waiting

    def flush(self):
        pass

    def flushInput(self):
        pass

    def flushOutput(self):
        pass


if not EMBEDDED:
    class list_ports(object):
        def comports(self, include_links=False):
            available = []

            possible_ports = []

            if sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
                possible_ports += glob.glob("/dev/rfcomm[0-9]*")
                possible_ports += glob.glob("/dev/ttyUSB[0-9]*")

            elif sys.platform.startswith('win'):
                possible_ports += ["\\.\COM%d" % i for i in range(256)]

            elif sys.platform.startswith('darwin'):
                exclude = [
                    '/dev/tty.Bluetooth-Incoming-Port',
                    '/dev/tty.Bluetooth-Modem'
                ]
                possible_ports += [port for port in glob.glob('/dev/tty.*') if port not in exclude]

            # possible_ports += glob.glob('/dev/pts/[0-9]*') # for obdsim

            for port in possible_ports:
                available.append(port)

            return available


    class Serial(SerialCommon):

        BAUD_MAP = {
            9600: termios.B9600,
            # From Linux asm-generic/termbits.h
            19200: 14,
            57600: termios.B57600,
            115200: termios.B115200
        }

        def __init__(self, port, baudrate, timeout=None, **kwargs):
            super(Serial, self).__init__(port, baudrate, timeout)
            self.port = port
            self._baudrate = baudrate
            self.timeout = -1 if timeout is None else timeout * 1000
            self.open()

        def open(self):
            self.fd = os.open(self.port, os.O_RDWR | os.O_NOCTTY)
            termios.setraw(self.fd)
            iflag, oflag, cflag, lflag, ispeed, ospeed, cc = termios.tcgetattr(self.fd)
            #print("tcgetattr result:", iflag, oflag, cflag, lflag, ispeed, ospeed, cc)
            baudrate = self.BAUD_MAP[self._baudrate]
            termios.tcsetattr(self.fd, termios.TCSANOW, [iflag, oflag, cflag, lflag, baudrate, baudrate, cc])
            self.poller = uselect.poll()
            self.poller.register(self.fd, uselect.POLLIN | uselect.POLLHUP)

        def close(self):
            os.close(self.fd)

        @property
        def in_waiting(self):
            buf = ustruct.pack('I', 0)
            fcntl.ioctl(self.fd, FIONREAD, buf, True)
            return ustruct.unpack('I', buf)[0]

        def write(self, data):
            return os.write(self.fd, data)

        def read(self, size=1):
            buf = b""
            c = 0
            while size > 0:
                if not self.poller.poll(self.timeout):
                    break
                chunk = os.read(self.fd, size)
                l = len(chunk)
                if l == 0:
                    # If we read 0 butes, it means that port is gone
                    # (for example, underlying hardware like USB adapter
                    # disconnected)
                    raise SerialDisconnectException("Port disconnected")
                size -= l
                buf += bytes(chunk)
                c += 1

            return buf


if EMBEDDED:
    class list_ports:
        def comports(self, include_links=False):
            return [1, 2]


    class Serial(SerialCommon):

        def __init__(self, port, baudrate, **kwargs):
            super(Serial, self).__init__(port, baudrate)
            self.port = port
            self._baudrate = baudrate
            self._kwargs = kwargs
            self.open()

        def open(self):
            self.fd = machine.UART(self.port, self._baudrate, **self._kwargs)

        def close(self):
            self.fd.deinit()

        @property
        def in_waiting(self):
            return self.fd.any()

        def write(self, data):
            return self.fd.write(data)

        def read(self, size=1):
            buf = self.fd.read(size)
            return buf

        def flushInput(self):
            while self.in_waiting:
                self.read()


class tools:
    # Common
    list_ports = list_ports

sys.modules['serial.tools'] = tools
