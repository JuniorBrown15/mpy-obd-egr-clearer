# This file is executed on every boot (including wake-boot from deepsleep)
#import esp
#esp.osdebug(None)
import os
import gc
import sys
sys.path.append('/flash/lib')

import time
import machine
from serial import Serial

led = None
uart_elm = None
uart_bt = None
obd = None
egr_clearer = None
dpf_monitor = None

logfile = open('log.txt', 'a+b')

try:
    from elm327.connection import SerialConnection
    from elm327.obd import OBDInterface
    from elm327.pcm_values import *
    from alfa_emissions import Alfa159Error
    from alfa_emissions.egr_clearer import EgrClearer
    from alfa_emissions.dpf_monitor import DpfMonitor


    def reboot(_):
        global logfile
        logfile.write("reboot\n")
        logfile.flush()
        logfile.close()
        machine.reset()

    def beep(duration=0.1, freq=2000, duty=90, pin=19):
        buzzer = machine.PWM(machine.Pin(pin, machine.Pin.OUT))
        buzzer.duty(duty)  # out of 100
        buzzer.freq(freq)
        time.sleep(duration)
        buzzer.duty(0)
        buzzer.deinit()

    def setup():
        global led
        global uart_elm
        global uart_bt
        global obd
        global egr_clearer
        global dpf_monitor
        global logfile

        boot = machine.Pin(0, machine.Pin.IN)
        boot.irq(trigger=machine.Pin.IRQ_FALLING, handler=reboot)

        led = machine.Pin(2, machine.Pin.OUT)
        led.value(0)

        # Numbers are GPIO number
        u1 = machine.UART(1, baudrate=38400, rx=22, tx=23, timeout=10)
        s2 = Serial(2, 38400, rx=16, tx=17)#, buffer_size=256)

        uart_elm = s2.fd
        uart_bt = u1

        obd = OBDInterface(SerialConnection(s2))

        egr_clearer = EgrClearer(obd, logfile)
        dpf_monitor = DpfMonitor(obd, logfile)

        for i in range(10):
            led.value(0 if led.value() else 1)
            time.sleep(0.1)

        def reflect(_):
            logn = 1
            logfn = "reflect_{}.log"
            while logfn.format(logn) in os.listdir('.'):
                logn += 1
            with open(logfn.format(logn), 'w') as reflog:
                start = time.time()
                while time.time() < (start + 30):
                    led.value(0 if led.value() else 1)
                    gc.collect()
                    while uart_bt.any():
                        start = time.time()
                        read = uart_bt.read().decode()
                        uart_elm.write(read)
                        reflog.write('>> ')
                        reflog.write(read.replace('\r', '\n'))
                    while uart_elm.any():
                        read = uart_elm.read().decode()
                        uart_bt.write(read)
                        reflog.write('<< ')
                        reflog.write(read.replace('\r', '\n'))
                reflog.write('\nFinished Reflecting\n')

        bt_act = machine.Pin(21, machine.Pin.IN)
        bt_act.irq(trigger=machine.Pin.IRQ_FALLING, handler=reflect)

        beep(duration=0.01)
        gc.collect()

    setup()

    egr_clearer.setup()

    last_regen = False

    while True:
        led.value(1)
        # if uart_bt.any():
        #     reflect()

        try:
            if egr_clearer.process():
                # beep(duration=0.06, freq=6000)
                pass
            led.value(0)

            if dpf_monitor.current_dpf_regen() and not last_regen:
                beep(duration=0.5)
                time.sleep(0.2)
                beep(duration=0.5)

                logfile.write("*** REGEN ***\n")
                # time.sleep(30)
                last_regen = True
            else:
                last_regen = False

        except Alfa159Error:
            # beep(duration=0.1, freq=600)
            # obd.send_command('AT LP')  # Send elm to sleep
            time.sleep(30)
            # uart_elm.write(' @\r')  # Should wake the elm
            egr_clearer.setup()

        logfile.flush()
        gc.collect()

except Exception as ex:
    sys.print_exception(ex, logfile)
    print(ex)

logfile.flush()
logfile.close()
print("Error, Rebooting")
time.sleep(2)
machine.reset()

